Feature: DemoApplication

Scenario Outline: To check the login function

Given open the browser
Then  click login button
And Enter the Email "<username>" in the textbox
And user has to enter the "<password>" in the textbox
Then click the login button
Then user has to click the logout

Examples:
|username              | password      |
|pavithra@gmail.com    | pragya@123    |
|pavipragya@gmail.com  | pragya@123    |
