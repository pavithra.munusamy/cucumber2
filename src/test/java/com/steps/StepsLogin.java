package com.steps;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class StepsLogin {
	static WebDriver driver;
	@Given("open the browser")
	public void open_the_browser() {
		driver = new ChromeDriver();
	    driver.get("https://demowebshop.tricentis.com/");
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	        
	    }

	@Then("click login button")
	public void click_login_button() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@Then("Enter the Email {string} in the textbox")
	public void enter_the_email_in_the_textbox(String string) {
		driver.findElement(By.id("Email")).sendKeys(string);
	}

	@Then("user has to enter the {string} in the textbox")
	public void user_has_to_enter_the_in_the_textbox(String string) {
		driver.findElement(By.name("Password")).sendKeys(string);
	}

	@Then("user has to click the logout")
	public void user_has_to_click_the_logout() {
		driver.findElement(By.linkText("Log out")).click();
	}

}
